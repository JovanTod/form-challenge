// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import StringInput from './components/StringInput.vue'
import RadioInput from './components/RadioInput.vue'
import SelectInput from './components/SelectInput.vue'
import EmailInput from './components/EmailInput.vue'
import PhoneInput from './components/PhoneInput.vue'
import MultiInput from './components/MultiInput.vue'
import App from './App.vue'

Vue.use(VueAxios, axios)

Vue.config.productionTip = false

Vue.component('string-input', StringInput)
Vue.component('select-input', SelectInput)
Vue.component('email-input', EmailInput)
Vue.component('radio-input', RadioInput)
Vue.component('phone-input', PhoneInput)
Vue.component('multi_string-input', MultiInput)
/* eslint-disable no-new */

new Vue({
  el: '#app',
  data () {
    return {
      formInputs: []
    }
  },
  components: {
    StringInput,
    SelectInput,
    EmailInput,
    RadioInput,
    PhoneInput,
    MultiInput
  },
  render: h => h(App)
})
